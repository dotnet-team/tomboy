<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:e="http://projectmallard.org/experimental/" type="topic" style="task" id="sync-notes" xml:lang="lv">

  <info>
    <link type="guide" xref="index#sync"/>
    <desc>Sinhronizē savas Tomboy piezīmes ar citiem datoriem.</desc>
    <revision pkgversion="3.0" version="0.1" date="2010-05-29" status="candidate"/>
    <credit type="author">
      <name>Paul Cutler</name>
      <email>pcutler@gnome.org</email>
    </credit>    
<!--
    <copyright>
      <year>2010</year>
      <name>GNOME Documentation Project</name>
    </copyright>
-->
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

 <title>Sinhronizē savas piezīmes</title>

 <section id="sync">

	<title>Pašrocīga piezīmju sinhronizācija</title>
	
	<p>Tu vari sinhronizēt savas piezīmes 3 veidos:</p>
	
	<list type="numbered">
	  <item><p>No jebkuras piezīmes, nospied <gui>Rīki</gui> ikonu un nospied <gui>Sinhronizēt piezīmes</gui>.</p></item>
	  <item><p><gui>Meklēt visas piezīmes</gui> izvēlnē, paņem <guiseq><gui>Rīki</gui><gui>Sinhronizēt piezīmes</gui></guiseq>.</p></item>
	  <item><p>Izmantojot savu peli, spied labo peles pogu uz Tomboy ikonas savā panelī vai dokā un izvēlies <gui>Sinhronizēt visas piezīmes</gui>.</p></item>
	</list>

 </section>
 
 <section id="autosync">
 
 	<title>Sinhronizē piezīmes automātiski</title>
 	
 	<p>Tomboy var automātiski fonā sinhronizēt tavas piezīmes. Ir ieteicams uzstādīt to, kā tu kontrolē piezīmju konfliktus, pirms automātiskās sinhronizācijas aktivēšanas. Par piezīmju konfliktu kontrolēšanu lasi <link xref="sync-setup#conflict"/> lapu.</p>
 	
 	<p>Tu vari aktivēt sinhronizāciju, atverot <gui>Tomboy Iestatījumi</gui>, izmantojot savu peli, lai veiktu labo klikšķi uz Tomboy ikonas savā panelī, uzdevumu joslā vai dokā. Izvēlies <gui>Iestatījumi</gui> un tad izvēlies <gui>Sinhronizācija</gui> cilni. Tu vari arī atvērt <gui>Tomboy iestatījumi</gui> no <gui>Meklēt visas piezīmes</gui> izvēlnes. No šīs izvēlnes paņem <guiseq><gui>Rediģēt</gui><gui>Iestatījumi</gui></guiseq>.</p>
	
	<p>Lai aktivētu automātisko sinhronizāciju, atzīmē <gui>Sinhronizēt piezīmes automātiski fonā katras x minūtes</gui> lauku, Sinhronizācijas cilnes lejā.</p>
	
	<p>Noklusētais automātiskās sinhronizācijas intervāls ir katras 10 minūtes. Lai pamainītu intervālu, cik bieži Tomboy vajadzētu sinhronizēt piezīmes, ievadi šo skaitli attiecīgajā laukā vai izmanto peli, lai virzītu laiku uz augšu vai leju.</p>
	
	<p>Lai deaktivētu automātisko sinhronizāciju, atslēdz <gui>Sinhronizēt piezīmes automātiski fonā katras x minūtes</gui> lauku.</p>
	
  </section>

 </page>
